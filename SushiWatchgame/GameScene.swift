//
//  GameScene.swift
//  SushiTower
//
//  Created by Parrot on 2019-02-14.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit
import WatchConnectivity
//new //
class GameScene: SKScene, WCSessionDelegate {
  
    var timer: Timer?
    
   var timercount = 20
  // var timerprogess: UIProgressView!
    
    var timerlabel: UILabel!
    var namescore: UITextField!

    
    var bestString:String?

    
    func sessionDidDeactivate(_ session: WCSession) {
        
        
    }
    
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
   
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any] ) {
        
        DispatchQueue.main.async {
            
         //   print("Message Received on GameScene\(message["message"]!)")
            
             let Countstr = message["count"] as! String
        
            let Firstname = message["name"] as! String

            if (Countstr == "0"){
                
            }
            else
            {
                self.timercount = self.timercount + 10
            }
            
            if(Firstname == "nochange")
            {
                
            }
            else{
//
            self.bestString = Firstname

            print("Phone: I received a message: \(self.bestString as String?)")

            if(Firstname == "Right"){
                print("Right")
                self.updatecattoright()
            }
            else {
                
                print("Left")
                self.updatecattoleft()


                
            }
            
        }
        }
    }
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")

    // Make a tower
    var sushiTower:[SushiPiece] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    var catPosition = "left"
    
    // Show life and score labels
    let lifeLabel = SKLabelNode(text:"Lives: ")
    let scoreLabel = SKLabelNode(text:"Score: ")
    
    var lives = 5
    var score = 0
    
    
    func spawnSushi() {
        
        // -----------------------
        // MARK: PART 1: ADD SUSHI TO GAME
        // -----------------------
        
        // 1. Make a sushi
        let sushi = SushiPiece(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
            
        }
        else {
            // OPTION 1 syntax: let previousSushi = sushiTower.last
            // OPTION 2 syntax:
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
    }
    func setProgress() {
        
       
        print("count 1")
        
        
    }
  
    func createTimer() {
        // 1
        if timer == nil {
            // 2
            timer = Timer.scheduledTimer(timeInterval: 1.0,
                                         target: self,
                                         selector: #selector(updateTimer),
                                         userInfo: nil,
                                         repeats: true)
        }
    }

  
    
    @objc func updateTimer() {
        
        self.sendmessagetowatch()
        
        if (lives == 0)  {
            
            timer?.invalidate()
            showAlert(withTitle: "Opps ", message: "Game over")
            
        }
       
        
        if (timercount == 0)  {
            
            timer?.invalidate()
            showAlert(withTitle: "Opps ", message: "Game over")
            
        }
        else
            
        if (timercount < 100)  {
            
            timercount = timercount - 1
            
            
            timerlabel.text = String(timercount)
        }
    
    


    }
    func sendmessagetowatch() {
        
        
        let timerstring: String = String(timercount)
        let message = ["time":
            timerstring] as! [String: Any]
        WCSession.default.sendMessage(message, replyHandler: nil)
    }
    override func didMove(to view: SKView) {
        
       
        
        
        
        
        // timerprogess = UIProgressView(frame: rect)
//        //cpuprogess.progress = 40.00
//       // timerprogess.trackTintColor = UIColor.red.withAlphaComponent(0.3)
//        //progressView.tintColor = UIColor.green.withAlphaComponent(1.0)
//        timerprogess.tintColor = UIColor.blue
//        timerprogess.transform = timerprogess.transform.scaledBy(x: 1, y: 15)
//        view.addSubview(timerprogess)
//
//        timerprogess.progress = Float(timerprogressValue)
       // let rect = CGRect(x: 10, y: 80, width: 250, height: 1000)
        
        let rect = CGRect(x: 60, y: 675, width: 250, height: 1000)

         self.timerlabel = UILabel(frame: rect)
      //  let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        self.timerlabel.center = CGPoint(x: 100, y: 100)
        self.timerlabel.textAlignment = .center
        self.timerlabel.text = "0"
        self.view!.addSubview(self.timerlabel)
        
        
        self.createTimer()


        
      
      //  targetViewController.view.addSubview(progressView)
        
        
        if (WCSession.isSupported() == true) {
           // labelshow.text = "WC is supported!"
            print("WC  supported!")

            
            // create a communication session with the watch
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            
            print("WC NOT supported!")
            //labelshow.text = "WC NOT supported!"
        }
        
        
        
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        
        // Game labels
        self.scoreLabel.position.x = 240
        self.scoreLabel.position.y = size.height - 100
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.fontSize = 20
       // self.score =

        addChild(scoreLabel)
        
        self.scoreLabel.text = "0"

        // Life label
        
        
        
        
        self.lifeLabel.position.x = 240
        self.lifeLabel.position.y = size.height - 150
        self.lifeLabel.fontName = "Avenir"
        self.lifeLabel.fontSize = 20
       // self.score =

        addChild(lifeLabel)
        
        
        
    }
    
    func buildTower() {
        for _ in 0...10 {
            self.spawnSushi()
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
    }
    
    func showAlert(withTitle title: String, message: String) {
        
        
        let xNSNumber = score as NSNumber
        let xString : String = xNSNumber.stringValue
        
        let alert = UIAlertController(title: "Game Over!! Your score is ", message: xString, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        //
                alert.addTextField(configurationHandler: { textField in
               // textField.placeholder = "Input your name here..."
                    self.namescore = alert.textFields![0] as UITextField
                    print("Text field: \(textField.text)")
                    
            })
        //        guard let startScene = SKScene(fileNamed: "GameScene") else { return } // fileNamed is the name you gave the .sks file
        //        self.view?.presentScene(startScene)
        
        //            if let scene = SKScene(fileNamed: "Gamescene") {
        //
        //
        //                scene.scaleMode = .aspectFill
        //                // OPTION 1: Change screens with an animation
        //                self.view?.presentScene(scene, transition: SKTransition.flipHorizontal(withDuration: 2.5))
        //                // OPTION 2: Change screens with no animation
        //                //self.view?.presentScene(scene)
        //            }
        // OPTION 1: Change screens with an animation
        
        
        
        
        alert.addAction(UIAlertAction(title: "Save Score", style: .default, handler: { action in
            
            print("Text field: \(self.namescore.text)" + String(self.score))
            
        }))
        
        
        // self.repositionanchor()
        //self.repositionanchor()
        
        //] self.gamerunning = true
        
        view?.window?.rootViewController?.present(alert, animated: true)

        }
    func updatecattoleft() {
        
        
        let pieceToRemove = self.sushiTower.first
        if (pieceToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // To make the tower inifnite, then ADD a new piece
            self.spawnSushi()
        }
        
        
        
        
        
        
        
        
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
        // ------------------------------------
        // MARK: WIN AND LOSE CONDITIONS
        // -------------------------------------
        if (self.sushiTower.count > 0) {
            // 1. if CAT and STICK are on same side - OKAY, keep going
            // 2. if CAT and STICK are on opposite sides -- YOU LOSE
            let firstSushi:SushiPiece = self.sushiTower[0]
            let chopstickPosition = firstSushi.stickPosition
            
            if (catPosition == chopstickPosition) {
                // cat = left && chopstick == left
                // cat == right && chopstick == right
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = LOSE")
                print("------")
                self.lives = self.lives - 1
                self.lifeLabel.text = "Lives: \(self.lives)"
            }
            else if (catPosition != chopstickPosition) {
                // cat == left && chopstick = right
                // cat == right && chopstick = left
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = WIN")
                print("------")
                
                self.score = self.score + 10
                self.scoreLabel.text = "Score: \(self.score)"
                print("winc socre is  = \(self.score)")

            }
        }
            
        else {
            print("Sushi tower is empty!")
        }
        
        
        
        
        print("TAP LEFT")
        // 2. person clicked left, so move cat left
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        // change the cat's direction
        let facingRight = SKAction.scaleX(to: 1, duration: 0)
        self.cat.run(facingRight)
        // save cat's position
        self.catPosition = "left"
    }
    func updatecattoright() {
        
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
        
        
        
        
        let pieceToRemove = self.sushiTower.first
        if (pieceToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // To make the tower inifnite, then ADD a new piece
            self.spawnSushi()
        }
        
        
        
        print("TAP RIGHT")
        // 2. person clicked right, so move cat right
        cat.position = CGPoint(x:self.size.width*0.85, y:100)
        // change the cat's direction
        let facingLeft = SKAction.scaleX(to: -1, duration: 0)
        self.cat.run(facingLeft)
        // save cat's position
        self.catPosition = "right"
        
        if (self.sushiTower.count > 0) {
            // 1. if CAT and STICK are on same side - OKAY, keep going
            // 2. if CAT and STICK are on opposite sides -- YOU LOSE
            let firstSushi:SushiPiece = self.sushiTower[0]
            let chopstickPosition = firstSushi.stickPosition
            
            if (catPosition == chopstickPosition) {
                // cat = left && chopstick == left
                // cat == right && chopstick == right
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = LOSE")
                print("------")
                self.lives = self.lives - 1
                self.lifeLabel.text = "Lives: \(self.lives)"
            }
            else if (catPosition != chopstickPosition) {
                // cat == left && chopstick = right
                // cat == right && chopstick = left
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = WIN")
                print("------")
                
                self.score = self.score + 10
                self.scoreLabel.text = "Score: \(self.score)"
                print("winc socre is  = \(self.score)")
                
            }
        }
            
        else {
            print("Sushi tower is empty!")
        }
        
        
        
        
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
//
//        // This is the shortcut way of saying:
//        //      let mousePosition = touches.first?.location
//        //      if (mousePosition == nil) { return }
//        guard let mousePosition = touches.first?.location(in: self) else {
//            return
//        }
//
//        print(mousePosition)
//
//        // ------------------------------------
//        // MARK: UPDATE THE SUSHI TOWER GRAPHICS
//        //  When person taps mouse,
//        //  remove a piece from the tower & redraw the tower
////        // -------------------------------------
////        let pieceToRemove = self.sushiTower.first
////        if (pieceToRemove != nil) {
////            // SUSHI: hide it from the screen & remove from game logic
////            pieceToRemove!.removeFromParent()
////            self.sushiTower.remove(at: 0)
////
////            // SUSHI: loop through the remaining pieces and redraw the Tower
////            for piece in sushiTower {
////                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
////            }
////
////            // To make the tower inifnite, then ADD a new piece
////            self.spawnSushi()
////        }
//        // ------------------------------------
//        // MARK: SWAP THE LEFT & RIGHT POSITION OF THE CAT
//        //  If person taps left side, then move cat left
//        //  If person taps right side, move cat right
//        // -------------------------------------
//        // 1. detect where person clicked
//        let middleOfScreen  = self.size.width / 2
//
//        if (mousePosition.x < middleOfScreen) {
//
//
////            print("TAP LEFT")
////            // 2. person clicked left, so move cat left
////            cat.position = CGPoint(x:self.size.width*0.25, y:100)
////            // change the cat's direction
////            let facingRight = SKAction.scaleX(to: 1, duration: 0)
////            self.cat.run(facingRight)
////            // save cat's position
////            self.catPosition = "left"
//        }
//        else {
////            print("TAP RIGHT")
////            // 2. person clicked right, so move cat right
////            cat.position = CGPoint(x:self.size.width*0.85, y:100)
////            // change the cat's direction
////            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
////            self.cat.run(facingLeft)
////            // save cat's position
////            self.catPosition = "right"
//        }
//        // ------------------------------------
//        // MARK: ANIMATION OF PUNCHING CAT
//        // ------------------------------------
//        // show animation of cat punching tower
//        let image1 = SKTexture(imageNamed: "character1")
//        let image2 = SKTexture(imageNamed: "character2")
//        let image3 = SKTexture(imageNamed: "character3")
//
//        let punchTextures = [image1, image2, image3, image1]
//
//        let punchAnimation = SKAction.animate(
//            with: punchTextures,
//            timePerFrame: 0.1)
//
//        self.cat.run(punchAnimation)
//        // ------------------------------------
//        // MARK: WIN AND LOSE CONDITIONS
//        // -------------------------------------
        if (self.sushiTower.count > 0) {
            // 1. if CAT and STICK are on same side - OKAY, keep going
            // 2. if CAT and STICK are on opposite sides -- YOU LOSE
            let firstSushi:SushiPiece = self.sushiTower[0]
            let chopstickPosition = firstSushi.stickPosition
//
            if (catPosition == chopstickPosition) {
                // cat = left && chopstick == left
                // cat == right && chopstick == right
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = LOSE")
                print("------")
                self.lives = self.lives - 1
                self.lifeLabel.text = "Lives: \(self.lives)"
            }
            else if (catPosition != chopstickPosition) {
                // cat == left && chopstick = right
                // cat == right && chopstick = left
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = WIN")
                print("------")

                self.score = self.score + 1
                self.scoreLabel.text = "Score: \(self.score)"
            }
        }

        else {
            print("Sushi tower is empty!")
        }

    }
//
}
