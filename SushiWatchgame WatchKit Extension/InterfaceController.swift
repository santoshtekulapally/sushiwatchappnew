//
//  InterfaceController.swift
//  SushiWatchgame WatchKit Extension
//
//  Created by santosh tekulapally on 2019-10-26.
//  Copyright © 2019 santosh tekulapally. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate  {
    var timer: Timer?

    @IBOutlet weak var powerupbutton: WKInterfaceButton!
    @IBOutlet weak var WelcomeLabel: WKInterfaceLabel!
    
    @IBOutlet weak var timershowlabel: WKInterfaceLabel!
    @IBAction func buttonleftpressed() {
        
        let  message  = ["name":"Left","count": "0"] as [String : Any]
        
        // Send the message
        WCSession.default.sendMessage(message, replyHandler:nil)
        
          WelcomeLabel.setText("WATCH: button left pressed:")

    }
    @IBAction func Buttonright() {
        
        print("Sending message to phone")
        if (WCSession.default.isReachable == true) {
            
//            let message = ["name":"Pritesh",
//                           "age":"20"] as [String : Any]
            
            let  message  = ["name":"Right","count": "0"] as [String : Any]
            
            // Send the message
            WCSession.default.sendMessage(message, replyHandler:nil)
            
        }
        else {
            
            WelcomeLabel.setText("cannot reach")
        }
        
    //    print("WATCH: button Right pressed: ")
        
        WelcomeLabel.setText("WATCH: button Right pressed:")

        
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    // Function to receive DICTIONARY from the watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Output message to terminal
        print("WATCH: I received a message: \(message)")
        
        let timecounter:String = message["time"] as! String

      //  if(timeLeft != "100"){
            
            if(timecounter == "0"){
                
                timershowlabel.setText("Game over")
                
            }
            else{
                
                timershowlabel.setText("\(timecounter) seconds left")
            }
            
            
       // }
        // Get the "name" key out of the dictionary
        // and show it in the label
//        let name = message["name"] as! String
//        let age = message["age"] as! Int
//        let id = message["id"] as! String
//
        
        
        
    }
    
    
    
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        self.powerupbutton.setHidden(true)
self.timershowlabel.setText("   Seconds left")
        print("---WATCH APP LOADED")
        
        self.createTimer()
        
        if (WCSession.isSupported() == true) {
          //  msgFromPhoneLabel.setText("WC is supported!")
            
            // create a communication session with the phone
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
           // msgFromPhoneLabel.setText("WC NOT supported!")
        }
    }
    func createTimer() {
        // 1
        if timer == nil {
            // 2
            timer = Timer.scheduledTimer(timeInterval: 2.0,
                                         target: self,
                                         selector: #selector(updateTimer),
                                         userInfo: nil,
                                         repeats: true)
        }
    }
    
     @objc func updateTimer() {
        
        let randomgenerator = Int.random(in: 1...3)
        if randomgenerator == 1
        {
            self.powerupbutton.setHidden(true)
            
        }
       else if randomgenerator == 2
        {
            self.powerupbutton.setHidden(true)

            
        }
        else{
            self.powerupbutton.setHidden(false)

        }
        
        
    }
    @IBAction func powerupbuttonpressee() {
        
        print("Sending message to phone")
        if (WCSession.default.isReachable == true) {
            
            //            let message = ["name":"Pritesh",
            //                           "age":"20"] as [String : Any]
            
            let  message  = ["name":"nochange","count": "10" ] as [String : Any]
            
            // Send the message
            WCSession.default.sendMessage(message, replyHandler:nil)
            
        }
        else{
            
        }
        
    }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
